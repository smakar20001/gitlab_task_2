import unittest
from calc import add 

class TestSumEvenNumbers(unittest.TestCase):
    def test_sum_even_numbers(self):
        self.assertEqual(add(1,2), 3) 

if __name__ == '__main__':
    unittest.main()
